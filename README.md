# Nick's PhD Dissertation

This dissertation has been many years in the making.

It has taken on several flavours over several years, and finally the flavouring is finished.

Herein lies the words yet to be written, the ideas waiting to be spoken

Why Phantasos?
- Phantasos presents images of earth, rock, water and wood
- Phantasos was one of the three Oneiroi (dreams)
- Phantasos was the Greek God of surreal dreams
