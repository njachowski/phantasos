Predictions should be both accurate and useful to be valuable
A prediction could be useful in theory but in practice only if it is accurate enough
An accurate prediction could be useful in theory but in practice only if it is useful enough
A framework for making both accurate and useful Predictions
Models/Predictions themselves can have success metrics to gauce the utility of that model over others and that utility metric is not necessarily positively correlated with model accuracy
Inaccurate models can also be useful if your goal is to mislead (e.g. an accurate model could disprove your point, give away too much information, etc) e.g. take food models like a McDonalds burger advertisement -- depicting an inaccurate model of the burger they're selling is probably better because people would probably be less likely to buy it if the photo was of an actual burger * it all depends on how you measure the success of your model

The problem: In academia I would often come across papers which dealt with models of unknown or unbelievable accuracy and limited utility. Similarly I realised that people look at maps and think that they are "true" without realising that maps are really just predictions. This led me down the rabbit hole of asking the question for my dissertation topic: how do we make more accurate models? how do we make more useful models? what do those words even mean? Then I went out into the real world of industry and worked for a few years and my questioning went further down the rabbit hole, wondering additionally about the value of predictions and models and how utility and accuracy are related to that, if at all.

I'm going to propose some debatable ideas about predictions and models related to the slippery concepts of accuracy, usefulness and value.


what is value? I might define value as one's ability to directly or indirectly profit from the usefulness of a model
let's look at some concrete examples:
in academia: value is often measured in units like publications, citations, degrees, tenure
e.g. a useful model is one that results in many publications that get cited many times helping to earn you a degree and eventually tenure
in industry: value is often measured in dollars
e.g. a useful model is one that earns your company more money than other models
can a model be useful yet not valuable? yes, if you can't find a way to profit from it but people still find it useful
in industry a good example of this is a free product which people use and love but the company can't figure out how to monetise it
usually such models are not sustainable in the long run, though there are notable exceptions and we might say that such products / models
have happened on an indirect form of profit or value creation that is beyond dollars and cents

In modeling we often have to make seemingly arbitrary decisions about models which can greatly affect the utility, accuracy and value of our models.
Some such arbitrary decisions in temporal spatial modeling include -- granularity, scale, range
And often these decisions are not discussed, debated, justified or even considered as part of the modeling process, rather they often follow the path of least resistance "what's the easiest? okay I'll do that"
The goal of this framework is to help uncover these arbitrary decisions and bring them into the modeling framework so that these major decisions which determine what it is you're actually modeling become more purposeful and in the end result in more valuable models
Additionally the framework deals explicitly with uncertainty to increase the accuracy of your models
And finally it provides a way to think about how to value different variations of similar models
